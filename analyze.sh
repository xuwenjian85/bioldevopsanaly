#!/bin/bash

set -euo pipefail

read1=data/SRR9666242_1.fastq.gz.small.fq.gz 
read2=data/SRR9666242_2.fastq.gz.small.fq.gz 
sample='test'
outdir=result
source $SETTINGS 

rnaseqc_image(){
    echo "add git"
    apk add --no-cache git &> /dev/null
    echo "git clone rnaseqc"
    git clone  --depth 1 --branch v${DOCKERIMGAE_RNASEQC_VERSION} https://github.com/getzlab/rnaseqc.git &> /dev/null
    sed -i 's/sklearn/scikit-learn/g' rnaseqc/Dockerfile
    echo "build rnaseqc image"
    docker build -t rnaseqc:${DOCKERIMGAE_RNASEQC_VERSION} -f rnaseqc/Dockerfile rnaseqc &> /dev/null
    rm -fr rnaseqc
}

htseq_image(){
    echo "build htseq image"
    docker build -t htseq:${DOCKERIMGAE_HTSEQ_VERSION} --build-arg HTSEQ_VERSION=${DOCKERIMGAE_HTSEQ_VERSION} -f htseq/Dockerfile htseq
}

# build required images

rnaseqc_image
htseq_image

star_version=${DOCKERIMGAE_STAR_VERSION}

mkdir -p ${outdir}/star
mkdir -p ${outdir}/rnaseqc
mkdir -p ${outdir}/htseq
mkdir -p ${outdir}/summary
chmod -R 777 ${outdir}

STAR="docker run -v ${PWD}:/media --rm -t quay.io/biocontainers/star:${DOCKERIMGAE_STAR_VERSION}"
SAMTOOLS="docker run -v ${PWD}:/media -u default --rm -t quay.io/biocontainers/samtools:${DOCKERIMGAE_SAMTOOLS_VERSION} samtools"
RNASEQC="docker run -v ${PWD}:/media --rm -t rnaseqc:${DOCKERIMGAE_RNASEQC_VERSION} rnaseqc"
HTSEQCOUNT="docker run -v ${PWD}:/media --rm -t htseq:${DOCKERIMGAE_HTSEQ_VERSION} htseq-count"

$STAR STAR --version
$SAMTOOLS --version
$RNASEQC --version

GENOME_FASTA=ref/chr21.fa
GTF_FILE=ref/gencode.v44lift37.basic.annotation.chr21_v2.gtf
GTF_collapse_FILE=ref/test_chr21.gtf
INDEX_DIRECTORY=ref/$(basename ${GENOME_FASTA})_${star_version}_index

## THIS file name is defined by STAR 
outbam=${outdir}/star/${sample}.Aligned.sortedByCoord.out.bam

# The STAR built index must match read length, specified by the sjdbOverhang parameter
# GDC only set this parameter

if [ ${build_index_star} == True  ]; then
	mkdir -p $INDEX_DIRECTORY
    chmod 777 $INDEX_DIRECTORY
	${STAR} STAR --runMode genomeGenerate \
		--runThreadN $cpu_threads \
		--genomeDir /media/$INDEX_DIRECTORY \
		--genomeFastaFiles /media/$GENOME_FASTA \
		--sjdbGTFfile /media/$GTF_FILE \
		--sjdbOverhang $OVERHANG \
        --outFileNamePrefix /media/$INDEX_DIRECTORY/
fi

if [ ${run_star} == True  ]; then

    $STAR STAR --twopassMode $twopassMode \
        --runThreadN ${cpu_threads} \
        --genomeDir /media/${INDEX_DIRECTORY} \
        --readFilesIn /media/${read1} /media/${read2} \
        --outFileNamePrefix /media/${outdir}/star/${sample}. \
        --limitBAMsortRAM ${RAM} \
        --readFilesCommand zcat \
        --alignSJoverhangMin $alignSJoverhangMin  \
        --alignSJDBoverhangMin $alignSJDBoverhangMin  \
        --outFilterMismatchNmax $outFilterMismatchNmax \
        --outFilterMismatchNoverLmax $outFilterMismatchNoverLmax \
        --alignIntronMin $alignIntronMin \
        --alignIntronMax $alignIntronMax \
        --alignMatesGapMax $alignMatesGapMax \
        --outFilterType	$outFilterType \
        --outFilterScoreMinOverLread $outFilterScoreMinOverLread \
        --outFilterMatchNminOverLread $outFilterMatchNminOverLread \
        --limitSjdbInsertNsj $limitSjdbInsertNsj \
        --outSAMstrandField $outSAMstrandField \
        --outFilterIntronMotifs None \
        --alignSoftClipAtReferenceEnds Yes \
        --quantMode $quantMode \
        --outSAMtype $outSAMtype \
        --outSAMunmapped $outSAMunmapped \
        --outSAMattrRGline $outSAMattrRGline \
        --outSAMattributes $outSAMattributes \
        --chimSegmentMin $chimSegmentMin \
        --chimJunctionOverhangMin $chimJunctionOverhangMin \
        --chimOutType $chimOutType \
        --chimMainSegmentMultNmax $chimMainSegmentMultNmax \
        --outReadsUnmapped Fastx \
        --outSJfilterReads $outSJfilterReads \
        --outSJfilterCountTotalMin $outSJfilterCountTotalMin \
        --sjdbScore $sjdbScore
        
    # --outSAMattrRGline ID:4 LB:lib1 PU:unit1 PL:illumina SM:sample1"
    cp ${outdir}/star/${sample}.Log.final.out ${outdir}/summary/${sample}.star.out

    $STAR rm -rf /media/${outdir}/star/${sample}._STAR*

    $SAMTOOLS index -@ ${cpu_threads} /media/${outbam}
    $SAMTOOLS flagstat /media/${outbam}  > ${outdir}/summary/${sample}.samtools.out

fi

if [ ${run_rnaseqc} == True  ]; then

    $RNASEQC --legacy --verbose --verbose /media/${GTF_collapse_FILE} /media/${outbam} /media/${outdir}/rnaseqc --coverage --sample=${sample}
    cp ${outdir}/rnaseqc/${sample}.metrics.tsv ${outdir}/summary/${sample}.rnaseqc.out
    cp ${outdir}/rnaseqc/${sample}.gene_reads.gct ${outdir}/summary/${sample}.gene_reads.gct
fi

if [ ${run_htseq} ]; then

    $HTSEQCOUNT -s no /media/${outbam} /media/$GTF_FILE -c /media/${outdir}/htseq/${sample}.tsv
    cp ${outdir}/htseq/${sample}.tsv ${outdir}/summary/${sample}.tsv

fi
