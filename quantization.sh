
#!/bin/bash
set -e -o pipefail

# load env
source $SETTINGS

outdir=result
sample=test

SAMTOOLS="docker run -v ${PWD}:/media -u default --rm -t quay.io/biocontainers/samtools:${DOCKERIMGAE_SAMTOOLS_VERSION} samtools"
media_outdir=/media/${outdir}

outbam=${media_outdir}/star/${sample}.Aligned.sortedByCoord.out.bam

# 1. genome mapping: STAR self log
cp ${outdir}/star/${sample}.Log.final.out ${outdir}/summary/${sample}.star.out
# 2. genome mapping: samtools log
$SAMTOOLS flagstat ${outbam}  > ${outdir}/summary/${sample}.samtools.out
# 3. read counting: rnaseqc self log metric
# cp ${outdir}/rnaseqc/${sample}.metrics.tsv ${outdir}/summary/${sample}.rnaseqc.out